# Build stage
FROM rust:latest as builder
WORKDIR /usr/src/myapp
# Copy the source code of your application
COPY . .
# Build your application
RUN cargo install --path .

# Final stage
FROM ubuntu:latest
# Copy the binary from the builder stage
COPY --from=builder /usr/local/cargo/bin/actix_web_app /usr/local/bin/actix_web_app
# Expose the port the application listens on
EXPOSE 8080
# Command to run the application
CMD ["actix_web_app"]
