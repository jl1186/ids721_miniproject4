# IDS 721 Miniproject 4 of Jamie

## Requirements: 
### Containerize a Rust Actix Web Service
- Containerize simple Rust Actix web app
- Build Docker image
- Run container locally
### Grading Criteria
- Container functionality: 40%
- Dockerfile and build: 40%
- Documentation: 20%
### Deliverables
Dockerfile
Screenshots or demo video showing container running
Writeup of process

## Steps

### I first created a new actix web app
```
cargo new actix_web_app
```

### Then I changed the dependency of Cargo.toml
```
[dependencies]
actix-web = "4"
actix-rt = "2"
```

### I then used a sample web server and wrote it in main.rs, and run on the localhost
```
cargo run
```

### Then, I created a Dockerfile and tried to run this app in Docker
```
docker build -t actix_web_app .
docker run -p 8080:8080 actix_web_app
```

That's how it works:
![Alt text](screenshot.png)

